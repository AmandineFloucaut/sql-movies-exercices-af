-- cleanup before re-creating
drop table if exists movies;
drop table if exists people;
drop table if exists roles;
drop table if exists reviewers;
drop table if exists reviews;

-- table creation
create table people
(
    id         int primary key,
    first_name text not null,
    last_name  text not null
);

create table movies
(
    id          int primary key,
    title       text not null,
    year        int,
    time        int,
    director_id int  not null references people (id)
);

create table roles
(
    actor_id       int references people (id),
    movie_id       int references movies (id),
    character_name text,
    PRIMARY KEY (actor_id, movie_id)
);

create table reviewers
(
    id       int primary key,
    nickname text not null
);

create table reviews
(
    movie_id    int references movies (id),
    reviewer_id int references reviewers (id),
    stars       int not null,
    PRIMARY KEY (movie_id, reviewer_id)
);
