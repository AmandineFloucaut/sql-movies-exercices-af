# Découverte des bases de données relationelles

## Prérequis

### Installation de docker

On va utiliser docker en mode "baguette magique" pour démarrer postgresql. Installer docker pour windows en le téléchargeant à l'adresse suivante : https://download.docker.com/win/static/stable/x86_64/docker-20.10.14.zip


### Installation de pgAdmin

pgAdmin est une interface d'administration pour postgresql. Ce programme nous permettra d'interagir avec nos bases de données via une interface graphique, sans s'embêter avec le Java dans un premier temps.

Il permet d'écrire des requêtes SQL (le langage qui permet d'intéragi avec la plupart des bases de données relationelles) mais aussi de parcourir la base de données en cliquant dans une interface graphique.

Installer pgAdmin pour windows à l'adresse suivante : https://ftp.postgresql.org/pub/pgadmin/pgadmin4/v6.7/windows/pgadmin4-6.7-x64.exe

### Démarrer postgresql avec docker

Docker permet de démarrer des **containers** à partir d'**images**. Une image est une sorte de "template" qui décrit la façon de lancer un programme. Le container est une instance isolée de ce programme.

Il y a une analogie possible avec la POO : Image = Classe, Container = Objet (ou instance)

Pour démarrer un container postgresql avec docker : taper la commande suivante dans un terminal : 

```
docker container run --name my-postgres -p5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres
```

Le container peut ensuite être stoppé / démarré avec les commandes suivantes (il faudra peut-être le démarrer après un reboot de l'ordinateur) : 

```
docker container stop my-postgres
docker container start my-postgres
```

### Se connecter à la base avec pgAdmin et créer la base de test

Démarrer pgAdmin et créer une nouvelle connexion en choisissant "Create Server". Les informations de connexion sont les suivantes : 

* Host : localhost
* Port : 5432
* username : postgres
* password : le mot de passe renseigné dans la commande docker de création du container.

Ouvrir une console sur le schéma "public" et jouer les deux scripts sql fournis à l'adresse suivante : https://gitlab.com/BenoitAverty/sql-movies-exercices

Vous pouvez explorer les tables et les données créées grâce à pgAdmin

## Requêtes SQL

### Sélection simple

1) Afficher tout le contenu de la table `movies`

> ```sql
> SELECT * FROM movies;
> ```

2) Afficher la liste des films triés par année de sortie croissante (Uniquement le nom des films doit apparaitre)

> ```sql
> SELECT title FROM movies ORDER BY year ASC;
> ```

3) Afficher la liste des personnes enregistrées en base (table `people`, uniquement les noms et prénoms)

> `SELECT first_name, last_name FROM people;`

4) Afficher la liste des films sortis avant les années 2000

> `SELECT * FROM movies WHERE year < 2000;`

5) Afficher la durée moyenne des films

> `SELECT AVG(time) FROM movies;`

### Jointures

1) Afficher la liste des films avec leur réalisateur

```sql
SELECT movies.title, people.first_name, people.last_name
FROM movies 
JOIN people 
ON movies.director_id=people.id;
```

2) Afficher la liste des films avec leur casting (Chaque film aura une ligne par personnage)

```sql
SELECT movies.title, people.first_name, people.last_name
FROM roles
JOIN people
ON people.id=roles.actor_id
JOIN movies
ON movies.id=roles.movie_id;

# ou

SELECT m.title, r.character_name, p.first_name, p.last_name
FROM  movies m,
      roles r,
      people p
WHERE r.movie_id = m.id
AND r.actor_id = p.id;
```

2) Afficher la liste des personnages du film "titanic" (Seulement le nom des personnages doit apparaitre)

```sql
SELECT roles.character_name
FROM roles
JOIN movies
ON roles.movie_id=movies.id
WHERE movies.title = 'Titanic';

# ou

SELECT r.character_name
FROM roles r
WHERE r.movie.id = (SELECT id FROM movies WHERE title = 'Titanic');
```

3) Afficher la liste des personnes qui ont réalisé un film (uniquement leur nom)

```sql
SELECT people.first_name, people.last_name
FROM people
JOIN movies
ON people.id = movies.director_id;

# ou

SELECT DISTINCT(last_name)
FROM people
WHERE id IN (SELECT director_id FROM movies);
```

4) Afficher la liste des notes du film "The Gentlemen"

```sql
SELECT reviews.stars, movies.title
FROM reviews
JOIN movies
ON reviews.movie_id=movies.id
WHERE movies.title = 'The Gentlemen';
```

5) Afficher la liste des reviewers n'ayant noté aucun film

```sql
SELECT reviewers.nickname
FROM reviewers
LEFT OUTER JOIN reviews
ON reviewers.id = reviews.reviewer_id
WHERE reviews.movie_id IS NULL;

# ou

SELECT reviewers.nickname
FROM reviewers
WHERE reviewers.id
NOT IN (SELECT reviewer_id FROM reviews);
```

### Grouper

1) Afficher la note moyenne de chaque film

```sql
SELECT movies.title, AVG(reviews.stars)
FROM movies
JOIN reviews
ON movies.id = reviews.movie_id
GROUP BY movies.title;
```

2) Afficher la liste des films triés par la note moyenne la plus forte

```sql
SELECT movies.title, AVG(reviews.stars)
FROM movies
JOIN reviews
ON movies.id = reviews.movie_id
GROUP BY movies.title
ORDER BY AVG(reviews.stars) DESC;
```

3) Afficher la liste des reviewers ayant noté tous les films ([doc HAVING](https://sql.sh/cours/having))

```SQL
SELECT reviewers.nickname
FROM reviewers
JOIN reviews
ON reviews.reviewer_id = reviewers.id
GROUP BY reviewers.nickname
HAVING COUNT(reviews.movie_id) >= (SELECT COUNT(DISTINCT movie_id) FROM reviews);
```

### Insérer des données

1) Insérer un film réalisé par Guy Ritchie dans la base de données

```sql
INSERT INTO movies(id, title, year, time, director_id)
VALUES('6', 'coucou', '2022', '190', 2);
```

2) Insérer un film réalisé par Lino Ventura dans la base de données

```sql
INSERT INTO people(id, first_name, last_name)
VALUES(19, 'Lino', 'Ventura');

INSERT INTO movies(id, title, year, time, director_id)
VALUES(7, 'LinoFilm', '2019', '160', 19);
```

3) Insérer le casting de ces deux films dans la base de données

```sql
INSERT INTO roles(actor_id, movie_id, character_name)
VALUES(3, 6, 'ThomasJavas');

INSERT INTO roles(actor_id, movie_id, character_name)
VALUES(8, 6, 'AlexJavas');

INSERT INTO roles(actor_id, movie_id, character_name)
VALUES(15, 6, 'CyriacJavas');

INSERT INTO roles(actor_id, movie_id, character_name)
VALUES(5, 6, 'AmandineJavas');

INSERT INTO roles(actor_id, movie_id, character_name)
VALUES(14, 7, 'AmandineCJavas');

INSERT INTO roles(actor_id, movie_id, character_name)
VALUES(11, 7, 'AmbreJavas');

INSERT INTO roles(actor_id, movie_id, character_name)
VALUES(13, 7, 'BaptisteJavas');

INSERT INTO roles(actor_id, movie_id, character_name)
VALUES(16, 7, 'MaximeJavas');
```

### Modifier/Supprimer des données

1) Modifier la note de "Hungry Jang" sur le film "Inglourious Basterds" à 3 étoiles

```sql
UPDATE reviews
SET stars = 3
WHERE movie_id = 1 AND reviewer_id = 1;
```

2) Supprimer la note de "Hungry Jang" sur le film "The Gentlemen"

```sql
DELETE FROM reviews
WHERE movie_id = 2 AND reviewer_id = 1;
```

3) Supprimer le film "Snatch" de la base de données, et toutes les données qui y sont liées

```sql
DELETE FROM roles
WHERE roles.movie_id = 5;

DELETE FROM reviews
WHERE reviews.movie_id = 5;

DELETE FROM movies
WHERE title = 'Snatch';
```

## Modélisation de base de données

On souhaite ajouter le support des séries TV à la base de données.

### Création de table

1) écrire la requête de création d'une table `tv_shows` pour stocker les séries. Cette table doit contenir les informations suivantes :
   * un ID numérique qui est la clé primaire
   * Un titre
   * Un nombre de saisons
   * Une année de début
   * Une année de fin (potentiellement vide si la série est en cours)

```sql
CREATE TABLE tv_shows (
   id int primary key not null,
   title text not null,
   number_seasons int not null,
   year_begining int not null,
   year_end int
);
```

2) écrire la requête de création d'une table `shows_episodes` pour stocker les épisodes d'une série. Cette table doit contenir les informations suivantes
   * Un numéro de saison
   * Un numéro d'épisode dans la saison
   * L'id de la série dont l'épisode fait partie (cette colonne doit référencer la colonne ID des séries TV)
   * Un titre
   * La clé primaire de cette table est constituée des trois premières colonnes

```sql
CREATE TABLE shows_episodes (
   season_number int,
   episode_number int,
   tv_show_id int REFERENCES tv_shows(id),
   title text,
   PRIMARY KEY (season_number, episode_number, tv_show_id)
);
```

3) Insérer quelques données dans ces tables.

```sql
INSERT INTO tv_shows(id, title, number_seasons, year_begining)
VALUES(1, 'lost', 8, 6);
```

4) BONUS : créer une table permettant de stocker des reviews sur des épisodes de séries.

## Documentation

* https://docs.postgresql.fr/12/
* https://docs.postgresql.fr/12/ddl.html définition de données
* https://docs.postgresql.fr/12/dml.html modification / insertion de données
* https://docs.postgresql.fr/12/queries.html
